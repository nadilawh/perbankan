/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author NAD
 */
public class StarterData {
    public List<NasabahModel> starterNasabah;
    public StarterData() {
        List<NasabahModel> dataNasabah = new ArrayList<NasabahModel>();
        for (int i = 0; i < 5; i++) {
            dataNasabah.add(new NasabahModel(
                    i+1, "Beny", "mygetzu" + i, "beny123 - " + i, new TabunganModel("M Beny P" + i, (20000 + (i * 1000)), (1400019980 + i))
            ));
        }
        
        this.starterNasabah = dataNasabah;
    }

    public List<NasabahModel> getStarterNasabah() {
        return starterNasabah;
    }

    public void setStarterNasabah(List<NasabahModel> starterNasabah) {
        this.starterNasabah = starterNasabah;
    }
    
}
