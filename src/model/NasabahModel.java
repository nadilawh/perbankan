/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author NAD
 */
public class NasabahModel extends AccountModel{
    private TabunganModel tabungan;
    
    public NasabahModel(int id, String nama, String username, String password, TabunganModel tab) {
        super(id, nama, username, password);
        this.tabungan = tab;
    }

    public TabunganModel getTabungan() {
        return tabungan;
    }

    public void setTabungan(TabunganModel tabungan) {
        this.tabungan = tabungan;
    }
    
}
